module.exports = {
  plugins: {
    'autoprefixer': {overrideBrowserslist: [ "iOS >= 8","Android > 4.4"]},
    'postcss-plugin-px2rem': {rootValue: 16,minPixelValue:1,exclude: /(node_modules)/}
  }
}
