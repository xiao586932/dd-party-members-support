import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [{
		path: '/',
		name: 'home',
		redirect: '/home',
		component: resolve => require(['@/views/home'], resolve),
		meta: {
			keepAlive: false,
			title: '帮扶对象走访'
		},
		children: [{
				path: '/home',
				name: 'index',
				component: resolve => require(['@/views/index'], resolve),
				meta: {
					keepAlive: false,
					title: '帮扶对象走访'
				}
			},
			{
				path: '/reported',
				name: 'reported',
				component: resolve => require(['@/views/events/reported'], resolve),
				meta: {
					keepAlive: false,
					title: '事件上报'
				}
			}
			// {
			// 	path: '/eventsReported',
			// 	name: 'events',
			// 	component: resolve => require(['@/views/events/events-reported'], resolve),
			// 	meta: {
			// 		keepAlive: true,
			// 		title: '事件上报'
			// 	}
			// }
		]
	},
	{
		path: '/userInfo',
		name: 'userInfo',
		component: resolve => require(['@/views/userInfo'], resolve),
		meta: {
			keepAlive: false,
			title: '党员帮扶人员走访'
		}
	}, {
		path: '/pz',
		name: 'pz',
		component: resolve => require(['@/views/pz'], resolve),
		meta: {
			keepAlive: true,
			title: '拍照'
		}
	}, {
		path: '/map',
		name: 'map',
		component: resolve => require(['@/views/map'], resolve),
		meta: {
			keepAlive: true,
			title: '地图定位'
		}
	}, {
		path: '/reported',
		name: 'reported',
		component: resolve => require(['@/views/events/reported'], resolve),
		meta: {
			keepAlive: false,
			title: '事件上报'
		}
	}, {
		path: '/txmap',
		name: 'txmap',
		component: resolve => require(['@/views/txmap'], resolve),
		meta: {
			keepAlive: true,
			title: '地图定位'
		}
	}, {
		path: '/historyList',
		name: 'historyList',
		component: resolve => require(['@/views/historyList'], resolve),
		meta: {
			keepAlive: false,
			title: '历史记录'
		}
	}, {
		path: '/bfcsjl',
		name: 'bfcsjl',
		component: resolve => require(['@/views/bfcsjl'], resolve),
		meta: {
			keepAlive: false,
			title: '帮扶措施记录'
		}
	}, {
		path: '/bljg',
		name: 'bljg',
		component: resolve => require(['@/views/bljg'], resolve),
		meta: {
			keepAlive: false,
			title: '办理经过'
		}
	}, {
		path: '/historyDetail',
		name: 'bljg',
		component: resolve => require(['@/views/historyDetail'], resolve),
		meta: {
			keepAlive: false,
			title: '历史详情'
		}
	}
]

const router = new VueRouter({
	routes
})

export default router
