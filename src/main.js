import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import util from './utils/util.js'
import api from './utils/api'
import '@/styles/index.less'
import '@/styles/iconfont/iconfont.css' //使用阿里图标库
import {VueJsonp} from 'vue-jsonp'

import {
	createApp
} from 'vue';
import Vant from 'vant';
import 'vant/lib/index.css';
//阻止启动生产消息
Vue.config.productionTip = false;

//公共方法
Vue.prototype.$util = util;

//公共api
Vue.prototype.$api = api;

//引入ajax请求方法
import {
	post,
	get,
	patch,
	put
} from './utils/http'
Vue.prototype.$post = post;
Vue.prototype.$get = get;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;

//全局注册过滤器
import filters from './utils/filters.js';
Object.keys(filters).forEach(key => {
	Vue.filter(key, filters[key])
})

// //解决click延迟问题
const FastClick = require('fastclick')
FastClick.attach(document.body)
Vue.use(Vant);
Vue.use(VueJsonp);
//全局引入vux提供的插件
// import {LoadingPlugin ,AlertPlugin, ConfirmPlugin, ToastPlugin} from 'vux'
// Vue.use(LoadingPlugin)
// Vue.use(AlertPlugin)
// Vue.use(ConfirmPlugin)
// Vue.use(ToastPlugin)
router.beforeEach((to, from, next) => {
	window.document.title = to.meta.title == undefined ? '党员帮扶' : to.meta.title
	next();
})

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
