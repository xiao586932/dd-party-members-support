import {get,post,deletefn} from './http.js'
export default {
  /* 
   * 规范保存接口地址：
   * xxxx: params => get/post('接口地址'，params), //这里请注释接口作用
   * 
   * 组件或页面调用时：
   * this.$api.xxxx(params).then().catch();
   */
  getConfig: params => post('/swhelp/config/?url=' + window.location.href ,params), //获取配置信息
  getUserInfo: params => post('/swhelp/login', params), // 获取用户信息
  getPeopleList: params => get('/swhelp/person/list',params), //获取对象列表
  getInterviewInfo: params=> post('/swhelp/interview/add',params), //获取党员走访信息
  eventsReported: params=> get('/swhelp/event/draft',params), //事件上报草稿
  eventPort: params=> post('/swhelp/event/report',params), //事件上报
  historyList: params=> get('/swhelp/interview/history',params), //查询走访历史记录列表
  historyDetail: params=> get('/swhelp/interview/history/detail',params), //查询走访历史记录详情
  addFamily: params=> post('/swhelp/person/home/add',params), //添加家庭成员
  getFamily: params=> get('/swhelp/person/home/list',params), //获取家庭成员
  editFamily: params=> post('/swhelp/person/home/update',params), //修改家庭成员
  removeFamily: params=> deletefn('/swhelp/person/home/delete',params), //删除家庭成员
  fileUpload: params=> post('/swhelp/common/upload',params), //附件上传
}